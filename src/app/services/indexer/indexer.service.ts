import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IndexerService {
  query = {};
  indexer;
  indexerUrl : string;
  indexerPassword : string;
  indexerUser : string;
  constructor(
    private http: HttpClient,
  ) {

    this.query = {
      query: '',
      page: { size : 4, current: 1 },
      filters: {
        all: [],
        any: [],
        none: []
      }
    };
    this.indexerUrl = environment.indexerUrl;
    this.indexerPassword = environment.indexerPassword;
    this.indexerUser = environment.indexerUser;
  }

  updateDocument(data): Observable<any> {
    const header = { Authorization: "Basic " + btoa(`${this.indexerUser}:${this.indexerPassword}`)}
    data.id = data.surveyId;
    const url = `${this.indexerUrl}/surveys/_doc/${data.surveyId}`;
    return this.http.post(url,data, { headers: header })
  }
  getDocument(data: string): Observable<any> {
    const header = { Authorization: "Basic " + btoa(`${this.indexerUser}:${this.indexerPassword}`)}
    this.query = {
      query: data,
    }
    const url = `${this.indexerUrl}/query_suggestion`;
    return  this.http.post(url, this.query, { headers: header })
  }
}
