import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VoteRoutingModule } from './vote-routing.module';
import { AddVoteComponent } from './add-vote/add-vote.component';
import { VoteComponent } from './vote/vote.component';
import { VoteDashboardComponent } from './vote-dashboard/vote-dashboard.component';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxNativeDateModule } from '@angular-material-components/datetime-picker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatRadioModule } from '@angular/material/radio'
import {MatDatepickerModule} from '@angular/material/datepicker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  declarations: [AddVoteComponent, VoteComponent, VoteDashboardComponent],
  imports: [
    CommonModule,
    VoteRoutingModule,
    NgxMatTimepickerModule,
    NgxMatDatetimePickerModule,
    NgbModule,
    FormsModule,
    MatDatepickerModule,
    MatRadioModule,
    NgxNativeDateModule,
    ReactiveFormsModule,
    ComponentsModule
  ]
})
export class VoteModule { }
