import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Card } from 'src/app/models/card';
import { Router } from '@angular/router';
import {NgbCarouselConfig, NgbCarousel} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import {
  chartOptions,
  parseOptions,
  chartExample2
} from "../../../variables/charts";
import Chart from 'chart.js';
import { User } from 'firebase';
import { SurveyChart } from '../../../models/survey-model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-vote-dashboard',
  templateUrl: './vote-dashboard.component.html',
  styleUrls: ['./vote-dashboard.component.scss'],
  providers: [NgbCarouselConfig]
})


export class VoteDashboardComponent implements OnInit {

  user: User = null;
  userinfo;
  surveyId;
  survey: any = null;
  surveyChart: SurveyChart = null;
  userSurveys;
  chart: Chart;
  participated;
  answer: string ='';
  voteAnswer;
  disable;

  constructor(private database : AngularFirestore,
    private authService: AuthService, private router : Router,
    private route: ActivatedRoute,  private toastr : ToastrService) { }

  ngOnInit(): void {
    this.disable =true;
    this.loadUser();
  }

  loadUser(){

    this.authService.currentUser.subscribe(
      data => {
        this.user = data;
        this.database.collection('user').doc(this.user.uid).get().subscribe(
          data => {
            this.userinfo = data.data();
          }
        );
      }
    )
    let labels = [], dataCount = [];
    this.route.params.subscribe(
      params => {
        this.surveyId = params.surveyId;
        this.database.collection('surveys').doc(params.surveyId).valueChanges().subscribe(
          data => {
            this.survey = data;
            if(this.survey.active){
              this.disable = false;
            }
            this.survey.answers.forEach(
              answer => {
                labels.push(answer.answer)
                dataCount.push(answer.votes)
              }
            )
            this.surveyChart = {
              labels: labels,
              datasets: [
                {
                  label: "Sales",
                  data: dataCount
                }
              ]
            }
            if (this.surveyChart !== null){
              var chartOrders = document.getElementById('chart-orders');
              parseOptions(Chart, chartOptions());
              this.chart = new Chart(chartOrders, {
                type: 'bar',
                options: chartExample2.options,
                data: this.surveyChart
              });
            }

          }
        )
      }
    )
  }

  endVote(){
    this.database.collection('surveys').doc(this.surveyId).update(
      {active : false, status : 'closed'}
      ).then(
      () => {
        this.disable = true;
        this.toastr.success('Your survey was succesfully closed');
      }
    )
  }
}
