import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDatepickerToggle } from '@angular/material/datepicker'
import { ToastrService } from 'ngx-toastr';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { IndexerService } from '../../../services/indexer/indexer.service'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProfilePicComponent } from 'src/app/components/profile-pic/profile-pic.component';
import { environment } from 'src/environments/environment';
import { AngularFireStorage } from '@angular/fire/storage';
@Component({
  selector: 'app-add-vote',
  templateUrl: './add-vote.component.html',
  styleUrls: ['./add-vote.component.scss']
})
export class AddVoteComponent implements OnInit {
  public answers = [];
  private user;
  public userinfo;
  public question = "";
  dayCounts: number;
  day;
  img = null;
  constructor(private authService: AuthService, private database: AngularFirestore, private _modalService: NgbModal,
    private toastr: ToastrService, private route: Router, private indexer: IndexerService, private storage : AngularFireStorage) { }

  ngOnInit(): void {
    this.answers = [
      {
        'answer': '',
        'votes': 0
      }
    ]
    this.authService.currentUser.subscribe(
      data => {
        this.user = data;
        this.database.collection('user').doc(this.user.uid).get().subscribe(
          data => {
            this.userinfo = data.data();
          }
        );
      }
    )
  }
  openPic() {
    const modalref = this._modalService.open(ProfilePicComponent);
    modalref.result.then(
      result => {
        this.img = result;
      }
    )
  }
  addAnswer() {
    if (this.answers.length <= 10) {
      this.answers.push({
        'answer': '',
        'votes': 0
      })
    } else {
      this.toastr.error('Max answers acquired');
    }
  }
  deleteAnswer() {
    if (this.answers.length > 1) {
      this.answers.pop()
    } else {
      this.toastr.error('Min answers acquired');
    }
  }
  uploadVote() {
    let today = new Date();
    if (this.dayCounts === undefined) {
      this.dayCounts = 1;
    }
    today.setDate(today.getDate() + this.dayCounts);
    let survey = {
      owner: this.user.uid,
      question: this.question,
      answers: this.answers,
      createDate: new Date(),
      endDate: today,
      status: 'active',
      active: true,
      usersVoted: 0,
      surveyId: '',
      picUrl: environment.defaultSurveyUrl
    }
    this.database.collection('surveys').add(survey).then(
      res => {
        this.database.collection('surveys').doc(res.id).update({
          surveyId: res.id
        })
        if (this.img !== null) {
          const profilePath = `Images/survey/${res.id}`;
          const fileRef = this.storage.ref(profilePath)
          this.toastr.info('Uploading your Survey Image wait until success to see');
          const task = fileRef.putString(this.img.base64.split(',')[1], 'base64').then(
            doc => {
              fileRef.getDownloadURL().subscribe(
                url => {
                  this.database.collection('surveys').doc(res.id).update({ picUrl: url }).then(
                    () => {
                      this.toastr.success('Finnish Uploading Survey Image reload to see the changes')
                    }
                  );
                }
              )
            }
          );
        }
        this.database.collection('user').doc(this.user.uid).collection('ownSurveys').add({
          surveyId: res.id,
          status: 'active'
        })
        this.database.collection('user').doc(this.user.uid).update({
          votationCreated: this.userinfo.votationCreated + 1
        })
        survey.surveyId = res.id;
        this.indexer.updateDocument(survey)
        this.toastr.success('Survey successfully created')
        this.route.navigate([`vote/${res.id}`])
      }
    )
  }
  getAnswer(event, index) {
    this.answers.forEach(
      (item, ind) => {
        if (ind === index) {
          item.answer = event.target.value;
        }
      })
  }

}
