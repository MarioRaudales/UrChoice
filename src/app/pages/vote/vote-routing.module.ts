import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddVoteComponent } from './add-vote/add-vote.component';
import { VoteDashboardComponent } from './vote-dashboard/vote-dashboard.component';
import { VoteComponent } from './vote/vote.component';


const routes: Routes = [
  {
    path: 'add-vote',
    component: AddVoteComponent
  },
  {
    path: 'voteDashboard/:surveyId',
    component: VoteDashboardComponent
  },
  {
    path: ':surveyId',
    component: VoteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VoteRoutingModule { }
