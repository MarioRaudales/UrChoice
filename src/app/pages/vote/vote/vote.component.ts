import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import {
  chartOptions,
  parseOptions,
  chartExample2
} from "../../../variables/charts";
import Chart from 'chart.js';
import { User } from 'firebase';
import { SurveyChart } from '../../../models/survey-model';
import { FormBuilder } from "@angular/forms";
import { MatRadioChange } from '@angular/material/radio'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.scss']
})

export class VoteComponent implements OnInit {
  user: User = null;
  userinfo;
  surveyId;
  survey: any = null;
  surveyChart: SurveyChart = null;
  userSurveys;
  chart: Chart;
  participated;
  answer: string ='';
  voteAnswer;
  voteDisabled;

  constructor(private route: ActivatedRoute, private authService: AuthService,
    public fb: FormBuilder, private database: AngularFirestore,
    private toastr : ToastrService, private router : Router) { }

    registrationForm = this.fb.group({
      gender: ['male']
    })
  ngOnInit(): void {
    this.voteDisabled =true;
    this.route.params.subscribe(
      params => {
        this.surveyId = params.surveyId;
        this.database.collection('surveys').doc(params.surveyId).get().subscribe(
          data => {
            this.survey = data.data();
            if (this.survey.endDate.toDate().getTime() <= new Date().getTime()){
              this.closeVote()
            }
            if (!this.survey.active || status === 'closed'){
              this.router.navigate([`vote/voteDashboard/${this.surveyId}`])
            }
          }
        )
      }
    )
    this.authService.currentUser.subscribe(
      data => {
        this.user = data;
        this.database.collection('user').doc(this.user.uid).valueChanges().subscribe(
          data => {
            this.userinfo = data
          }
        );
        this.database.collection('user').doc(this.user.uid).collection('paricipatedSurveys').doc(
          this.surveyId
        ).get().subscribe(
          data => {
            if(data.data() !== undefined){
              this.router.navigate([`vote/voteDashboard/${this.surveyId}`])
            }else{
              this.voteDisabled = false;
            }
          }
        )
      }
    )
  }
  public updateOptions() {
    this.chart.data.datasets[0].data = this.surveyChart;
    this.chart.update();
  }
  radioChange(event: MatRadioChange, data) {
    this.voteAnswer = data;
  }
  vote(){
    this.voteDisabled = true;
    const uservoted = this.survey.usersVoted;
    this.voteAnswer.votes = this.voteAnswer.votes + 1;
    let newVote = [];
    this.survey.answers.forEach(answer => {
      if(answer.answer !== this.voteAnswer.answer){
        newVote.push(answer)
      }
    });
    newVote.push(this.voteAnswer)
    this.database.collection('surveys').doc(this.surveyId).update(
      {answers : newVote, usersVoted : uservoted + 1}
    ).then(
      () => {
        this.toastr.success('Your vote was registered');
      }
    )
    this.database.collection('user').doc(this.user.uid).update({
      votationCounts: this.userinfo.votationCounts+1
    })
    this.database.collection('user').doc(this.user.uid).collection('paricipatedSurveys').doc(
      this.surveyId
    ).set(
      {
        paticipated: true,
        date: new Date(),
        vote: this.voteAnswer.answer
      }
    ).then(
      () => {
        this.router.navigate([`vote/voteDashboard/${this.surveyId}`])
      }
    );
  }
  closeVote(){
    this.database.collection('surveys').doc(this.surveyId).update(
      {active : false, status : 'closed'}
      ).then(
      () => {
        this.router.navigate([`vote/voteDashboard/${this.surveyId}`]);
      }
    )
  }
}
