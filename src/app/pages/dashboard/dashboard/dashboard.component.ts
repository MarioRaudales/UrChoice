import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from "../../../variables/charts";
import { AuthService } from 'src/app/services/auth/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Card } from 'src/app/models/card';
import {NgbCarouselConfig, NgbCarousel} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [NgbCarouselConfig]
})
export class DashboardComponent implements OnInit {

  surveys;
  Usersurveys = [];
  recentSurveysCarrousel : Card[];
  recentSurveys : Card[];
  highSurveys : Card[];
  public datasets: any;
  public data: any;
  public salesChart;
  public clicked: boolean = true;
  public clicked1: boolean = false;
  user = null;


  constructor(private database : AngularFirestore, private authService: AuthService, private router : Router) { }

  ngOnInit() {
    this.recentSurveys = [];
    this.recentSurveysCarrousel = [];
    this.highSurveys = [];
    this.loadUser();
    this.loadData();
    this.loadCharts();

  }

  public updateOptions() {
    this.salesChart.data.datasets[0].data = this.data;
    this.salesChart.update();
  }
  private loadUser(){
    this.user = this.authService.currentUser.subscribe(
      res => {
        this.user = res;
        this.database.collection('surveys', ref => ref.limit(6).where('owner','==',this.user.uid).where('active','==', true)).get().forEach(
          res => {
            res.forEach(
              doc => {
                const data = doc.data()
                this.Usersurveys.push(new Card(data.question,data.usersVoted,data.picUrl,data.surveyId))
              }
            )
          }
        )
      }
    )
  }
  private loadData(){
    this.database.collection('surveys', ref => ref.limit(12).orderBy('endDate',"desc").where('active','==', true)).valueChanges().forEach(
      res => res.forEach(
        doc => {
          const data : any = doc;
          this.recentSurveysCarrousel.push(new Card(data.question,data.usersVoted,data.picUrl,data.surveyId));
          if (this.recentSurveys.length < 6){
            this.recentSurveys.push(new Card(data.question,data.usersVoted,data.picUrl,data.surveyId));
          }
        }
      )
    )
    this.database.collection('surveys', ref => ref.limit(6).orderBy('usersVoted',"desc").where('active','==', true)).valueChanges().forEach(
      res => res.forEach(
        doc => {
          const data : any =  doc;

          this.highSurveys.push(new Card(data.question,data.usersVoted,data.picUrl,data.surveyId))
        }
      )
    )
  }
  private loadCharts(){
    this.datasets = [
      [0, 20, 10, 30, 15, 40, 20, 60, 60],
      [0, 20, 5, 25, 10, 30, 15, 40, 40]
    ];
    this.data = this.datasets[0];

    var chartOrders = document.getElementById('chart-orders');

    parseOptions(Chart, chartOptions());

    var ordersChart = new Chart(chartOrders, {
      type: 'bar',
      options: chartExample2.options,
      data: chartExample2.data
    });

    var chartSales = document.getElementById('chart-sales');

    this.salesChart = new Chart(chartSales, {
			type: 'line',
			options: chartExample1.options,
			data: chartExample1.data
		});
  }
  gotoSurvey(card){
    this.router.navigate([`vote/${card.identifier}`])
  }


}
