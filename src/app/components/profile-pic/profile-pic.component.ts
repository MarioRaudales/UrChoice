import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-profile-pic',
  templateUrl: './profile-pic.component.html',
  styleUrls: ['./profile-pic.component.scss']
})
export class ProfilePicComponent implements OnInit {
  @Input() imageChangedEvent;
  @Output() uploadImage = new EventEmitter();
  constructor(public modal: NgbActiveModal) { }

  ngOnInit(): void {
  }
  croppedImage: any = '';

  fileChangeEvent(event: any): void {
      this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
  }
  imageLoaded() {
      // show cropper
  }
  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }
  lanzar(){
    // Usamos el método emit
    this.modal.close({base64 : this.croppedImage, file: this.imageChangedEvent});
    
  }

}
