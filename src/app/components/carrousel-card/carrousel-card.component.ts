import { Component, OnInit, Input } from '@angular/core';
import { Card } from 'src/app/models/card';

@Component({
  selector: 'app-carrousel-card',
  templateUrl: './carrousel-card.component.html',
  styleUrls: ['./carrousel-card.component.scss']
})
export class CarrouselCardComponent implements OnInit {
  
  @Input() card : Card;
  constructor() { }

  ngOnInit(): void {
  }

}
