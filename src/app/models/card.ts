export class Card {
    cardTitle: string;
    subtitle: string;
    picUrl: string;
    identifier: string;
    constructor(
        cardTitle= '',
        subtitle= '',
        picUrl = '',
        identifier = '',
    ) {
        this.cardTitle = cardTitle;
        this.subtitle = subtitle;
        this.picUrl = picUrl;
        this.identifier = identifier;
    }
}