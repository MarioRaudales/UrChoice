// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBFpMLuQpJO4UxIyMvazw74EJ9-mCOoOV0",
    authDomain: "urchoice-fcbcd.firebaseapp.com",
    databaseURL: "https://urchoice-fcbcd.firebaseio.com",
    projectId: "urchoice-fcbcd",
    storageBucket: "urchoice-fcbcd.appspot.com",
    messagingSenderId: "533056804894",
    appId: "1:533056804894:web:f09a01c0dfed2e0b43360f",
    measurementId: "G-4THMDZF5FF"
  },
  indexerUser: 'elastic',
  indexerPassword: 'oahYiukug8RN95WyXRHW1Jbu',
  indexerUrl: 'https://862edcf1a1c64b899b2f4f9cba692ef7.us-central1.gcp.cloud.es.io:9243',
  defaultSurveyUrl: 'https://firebasestorage.googleapis.com/v0/b/urchoice-fcbcd.appspot.com/o/Assets%2Fquestion.jpg?alt=media&token=d9ba55f5-9c88-492a-b524-e20967002614'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
